using System;
//using System.Collections.Generic;
//using System.Text;
using System.Runtime.InteropServices;

namespace true_delete.Shredder
{
    public class cWin32
    {
        private const int VER_PLATFORM_WIN32_NT = 0x2;

        [StructLayout(LayoutKind.Sequential)]
        private struct OSVERSIONINFO
        {
            internal int dwVersionInfoSize;
            internal int dwMajorVersion;
            internal int dwMinorVersion;
            internal int dwBuildNumber;
            internal int dwPlatformId;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 127)]
            internal byte[] szCSDVersion;
        }

        [DllImport("shell32.dll", CharSet = CharSet.Auto)]
        public static extern int ShellExecute(IntPtr hwnd, string lpOperation, string lpFile, string lpParameters, string lpDirectory, int nShowCmd);


        [DllImport("kernel32", EntryPoint = "GetVersionEx")]
        private static extern bool GetVersionEx(ref OSVERSIONINFO osvi);

        /// <summary>
        /// Tests minimun OS required
        /// </summary>
        /// <returns>bool</returns>
        public bool VersionCheck()
        {
            OSVERSIONINFO tVer = new OSVERSIONINFO();
            tVer.dwVersionInfoSize = Marshal.SizeOf(tVer);
            GetVersionEx(ref tVer);
            if ((tVer.dwPlatformId & VER_PLATFORM_WIN32_NT) == VER_PLATFORM_WIN32_NT)
            {
                return true;
            }
            return false;
        }
    }
}
